# Aportes a Datos de Miércoles

En este repositorio estaré subiendo los scripts y de ser necesario otra información que use para mis aportes al #DatosDeMiercoles promovido por el movimiento de Ciencia de datos en Español.

Los datos principales para cada semana están disponibles en el repositorio de [Ciencia de datos en español](https://github.com/cienciadedatos/datos-de-miercoles) en Github.

Este repositorio se puede usar bajo licencia [GNU GPLv3](https://gitlab.com/gavg712/datos_de_miercoles_aportes/raw/master/LICENSE)